/*
  gcc -Wall  p.c -lm 
  ./a.out

What precision do I need for zoom ?
Mandelbreot set and DEM/M
https://en.wikibooks.org/wiki/Fractals/Computer_graphic_techniques/2D/plane#Zoom



uses the code from :
 git clone http://code.mathr.co.uk/book.git
unnamed c program[1] by Claude Heiland-Allen


https://gitlab.com/adammajewski/zoom_precision

git add p.c
git commit -m "aaa"
git push -u origin master


*/


#include <stdio.h>
#include <stdlib.h>
#include <math.h> // log2l



 // int iWidth = 1000;
  int iHeight = 1000;
  




int PrintInfoAbout_pixel_spacing_bits(int pixel_spacing_bits)
{
  printf("pixel_spacing_bits = -log2l( lPixelSpacing) = %d so ", pixel_spacing_bits);
  if (pixel_spacing_bits <= 40) { printf("one can use float, but it can be slower then double \n"); return 0;}
  if (pixel_spacing_bits > 60)  
      {printf("use arbitrary precision number (like MPFR) with %d bits precision \n", pixel_spacing_bits);
      return 0; }
  
  if (pixel_spacing_bits > 50)   
       {printf("use long double \n");
        return 0; }

  if (pixel_spacing_bits > 40)  
       printf("use double \n");
  
 return 0;
}


int TestZoom(int iHeight, long double lRadius, int iMax){

        
  	long double lPixelSpacing ; // = pixel size 
  	int iPixelSpacingBits ;  // precision in binary digits or bits 
  	long double lMagnification;
	int i;

	for ( i = 1; i < iMax; ++i)
	{

  		lMagnification = 1.0/lRadius; 
  		lPixelSpacing = lRadius / (iHeight / 2.0); // = pixel size 
  		iPixelSpacingBits = -log2l( lPixelSpacing); // 
  		printf("Magnification = %12Le ; radius = %12Le ; PixelSpacing = %12Le ; PixelSpacingBits = %d \n", lMagnification, lRadius, lPixelSpacing, iPixelSpacingBits);
  		PrintInfoAbout_pixel_spacing_bits(iPixelSpacingBits) ;
  		// zoom = decrease radius 
  		lRadius /=2.0;
	}
  
	return 0;   
}


int Compute_iPixelSpacingBits(int iHeight, long double lRadius){

	return -log2l(lRadius / (iHeight / 2.0));

}



int Test(int iHeight, long double lRadius){
	int iPixelSpacingBits ;  // precision in binary digits or bits 

	iPixelSpacingBits = Compute_iPixelSpacingBits(iHeight, lRadius); // 
	printf("For iHeight = %d and lRadius = %.16Le the  iPixelSpacingBits = %d \n", iHeight, lRadius, iPixelSpacingBits);
  	PrintInfoAbout_pixel_spacing_bits(iPixelSpacingBits);
  	printf("\n\n");
  	return 0;

}






int main() {
  
 
	Test(iHeight, 2.158333333333333e-16);
	TestZoom(iHeight, 2.0, 100);
  


 return 0;
}
