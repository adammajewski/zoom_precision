What precision do I need for zoom ?  
Mandelbreot set and DEM/M  

See:
* [wikibooks](https://en.wikibooks.org/wiki/Fractals/Computer_graphic_techniques/2D/plane#Zoom)



uses the code from :
*  [unnamed c program by Claude Heiland-Allen]( http://code.mathr.co.uk/book.git)


# c program
[p.c](p.c)


## function Test

Input:
* int iHeight = height of [the image](https://en.wikibooks.org/wiki/Fractals/Computer_graphic_techniques/2D/gfile) ( or the  [array](https://en.wikibooks.org/wiki/Fractals/Computer_graphic_techniques/2D)) in pixels 
* long double [lRadius = "the difference in imaginary coordinate between the center and the top of the axis-aligned view rectangle".](https://en.wikibooks.org/wiki/Fractals/Computer_graphic_techniques/2D/plane#radius)


Example output :   

```bash
For iHeight = 1000 and lRadius = 2.1583333333333330e-16 the  iPixelSpacingBits = 61 
pixel_spacing_bits = -log2l( lPixelSpacing) = 61 so use arbitrary precision number (like MPFR) with 61 bits precision 
```

## function TestZoom


Example output :   

```bash

Magnification = 5.000000e-01 ; radius = 2.000000e+00 ; PixelSpacing = 4.000000e-03 ; PixelSpacingBits = 7 
pixel_spacing_bits = -log2l( lPixelSpacing) = 7 so one can use float, but it can be slower then double 
Magnification = 1.000000e+00 ; radius = 1.000000e+00 ; PixelSpacing = 2.000000e-03 ; PixelSpacingBits = 8 
pixel_spacing_bits = -log2l( lPixelSpacing) = 8 so one can use float, but it can be slower then double 
Magnification = 2.000000e+00 ; radius = 5.000000e-01 ; PixelSpacing = 1.000000e-03 ; PixelSpacingBits = 9 
pixel_spacing_bits = -log2l( lPixelSpacing) = 9 so one can use float, but it can be slower then double 
Magnification = 4.000000e+00 ; radius = 2.500000e-01 ; PixelSpacing = 5.000000e-04 ; PixelSpacingBits = 10 
pixel_spacing_bits = -log2l( lPixelSpacing) = 10 so one can use float, but it can be slower then double 
Magnification = 8.000000e+00 ; radius = 1.250000e-01 ; PixelSpacing = 2.500000e-04 ; PixelSpacingBits = 11 
pixel_spacing_bits = -log2l( lPixelSpacing) = 11 so one can use float, but it can be slower then double 
Magnification = 1.600000e+01 ; radius = 6.250000e-02 ; PixelSpacing = 1.250000e-04 ; PixelSpacingBits = 12 
pixel_spacing_bits = -log2l( lPixelSpacing) = 12 so one can use float, but it can be slower then double 
Magnification = 3.200000e+01 ; radius = 3.125000e-02 ; PixelSpacing = 6.250000e-05 ; PixelSpacingBits = 13 
pixel_spacing_bits = -log2l( lPixelSpacing) = 13 so one can use float, but it can be slower then double 
Magnification = 6.400000e+01 ; radius = 1.562500e-02 ; PixelSpacing = 3.125000e-05 ; PixelSpacingBits = 14 
pixel_spacing_bits = -log2l( lPixelSpacing) = 14 so one can use float, but it can be slower then double 
Magnification = 1.280000e+02 ; radius = 7.812500e-03 ; PixelSpacing = 1.562500e-05 ; PixelSpacingBits = 15 
pixel_spacing_bits = -log2l( lPixelSpacing) = 15 so one can use float, but it can be slower then double 
Magnification = 2.560000e+02 ; radius = 3.906250e-03 ; PixelSpacing = 7.812500e-06 ; PixelSpacingBits = 16 
pixel_spacing_bits = -log2l( lPixelSpacing) = 16 so one can use float, but it can be slower then double 
Magnification = 5.120000e+02 ; radius = 1.953125e-03 ; PixelSpacing = 3.906250e-06 ; PixelSpacingBits = 17 
pixel_spacing_bits = -log2l( lPixelSpacing) = 17 so one can use float, but it can be slower then double 
Magnification = 1.024000e+03 ; radius = 9.765625e-04 ; PixelSpacing = 1.953125e-06 ; PixelSpacingBits = 18 
pixel_spacing_bits = -log2l( lPixelSpacing) = 18 so one can use float, but it can be slower then double 
Magnification = 2.048000e+03 ; radius = 4.882812e-04 ; PixelSpacing = 9.765625e-07 ; PixelSpacingBits = 19 
pixel_spacing_bits = -log2l( lPixelSpacing) = 19 so one can use float, but it can be slower then double 
Magnification = 4.096000e+03 ; radius = 2.441406e-04 ; PixelSpacing = 4.882812e-07 ; PixelSpacingBits = 20 
pixel_spacing_bits = -log2l( lPixelSpacing) = 20 so one can use float, but it can be slower then double 
Magnification = 8.192000e+03 ; radius = 1.220703e-04 ; PixelSpacing = 2.441406e-07 ; PixelSpacingBits = 21 
pixel_spacing_bits = -log2l( lPixelSpacing) = 21 so one can use float, but it can be slower then double 
Magnification = 1.638400e+04 ; radius = 6.103516e-05 ; PixelSpacing = 1.220703e-07 ; PixelSpacingBits = 22 
pixel_spacing_bits = -log2l( lPixelSpacing) = 22 so one can use float, but it can be slower then double 
Magnification = 3.276800e+04 ; radius = 3.051758e-05 ; PixelSpacing = 6.103516e-08 ; PixelSpacingBits = 23 
pixel_spacing_bits = -log2l( lPixelSpacing) = 23 so one can use float, but it can be slower then double 
Magnification = 6.553600e+04 ; radius = 1.525879e-05 ; PixelSpacing = 3.051758e-08 ; PixelSpacingBits = 24 
pixel_spacing_bits = -log2l( lPixelSpacing) = 24 so one can use float, but it can be slower then double 
Magnification = 1.310720e+05 ; radius = 7.629395e-06 ; PixelSpacing = 1.525879e-08 ; PixelSpacingBits = 25 
pixel_spacing_bits = -log2l( lPixelSpacing) = 25 so one can use float, but it can be slower then double 
Magnification = 2.621440e+05 ; radius = 3.814697e-06 ; PixelSpacing = 7.629395e-09 ; PixelSpacingBits = 26 
pixel_spacing_bits = -log2l( lPixelSpacing) = 26 so one can use float, but it can be slower then double 
Magnification = 5.242880e+05 ; radius = 1.907349e-06 ; PixelSpacing = 3.814697e-09 ; PixelSpacingBits = 27 
pixel_spacing_bits = -log2l( lPixelSpacing) = 27 so one can use float, but it can be slower then double 
Magnification = 1.048576e+06 ; radius = 9.536743e-07 ; PixelSpacing = 1.907349e-09 ; PixelSpacingBits = 28 
pixel_spacing_bits = -log2l( lPixelSpacing) = 28 so one can use float, but it can be slower then double 
Magnification = 2.097152e+06 ; radius = 4.768372e-07 ; PixelSpacing = 9.536743e-10 ; PixelSpacingBits = 29 
pixel_spacing_bits = -log2l( lPixelSpacing) = 29 so one can use float, but it can be slower then double 
Magnification = 4.194304e+06 ; radius = 2.384186e-07 ; PixelSpacing = 4.768372e-10 ; PixelSpacingBits = 30 
pixel_spacing_bits = -log2l( lPixelSpacing) = 30 so one can use float, but it can be slower then double 
Magnification = 8.388608e+06 ; radius = 1.192093e-07 ; PixelSpacing = 2.384186e-10 ; PixelSpacingBits = 31 
pixel_spacing_bits = -log2l( lPixelSpacing) = 31 so one can use float, but it can be slower then double 
Magnification = 1.677722e+07 ; radius = 5.960464e-08 ; PixelSpacing = 1.192093e-10 ; PixelSpacingBits = 32 
pixel_spacing_bits = -log2l( lPixelSpacing) = 32 so one can use float, but it can be slower then double 
Magnification = 3.355443e+07 ; radius = 2.980232e-08 ; PixelSpacing = 5.960464e-11 ; PixelSpacingBits = 33 
pixel_spacing_bits = -log2l( lPixelSpacing) = 33 so one can use float, but it can be slower then double 
Magnification = 6.710886e+07 ; radius = 1.490116e-08 ; PixelSpacing = 2.980232e-11 ; PixelSpacingBits = 34 
pixel_spacing_bits = -log2l( lPixelSpacing) = 34 so one can use float, but it can be slower then double 
Magnification = 1.342177e+08 ; radius = 7.450581e-09 ; PixelSpacing = 1.490116e-11 ; PixelSpacingBits = 35 
pixel_spacing_bits = -log2l( lPixelSpacing) = 35 so one can use float, but it can be slower then double 
Magnification = 2.684355e+08 ; radius = 3.725290e-09 ; PixelSpacing = 7.450581e-12 ; PixelSpacingBits = 36 
pixel_spacing_bits = -log2l( lPixelSpacing) = 36 so one can use float, but it can be slower then double 
Magnification = 5.368709e+08 ; radius = 1.862645e-09 ; PixelSpacing = 3.725290e-12 ; PixelSpacingBits = 37 
pixel_spacing_bits = -log2l( lPixelSpacing) = 37 so one can use float, but it can be slower then double 
Magnification = 1.073742e+09 ; radius = 9.313226e-10 ; PixelSpacing = 1.862645e-12 ; PixelSpacingBits = 38 
pixel_spacing_bits = -log2l( lPixelSpacing) = 38 so one can use float, but it can be slower then double 
Magnification = 2.147484e+09 ; radius = 4.656613e-10 ; PixelSpacing = 9.313226e-13 ; PixelSpacingBits = 39 
pixel_spacing_bits = -log2l( lPixelSpacing) = 39 so one can use float, but it can be slower then double 
Magnification = 4.294967e+09 ; radius = 2.328306e-10 ; PixelSpacing = 4.656613e-13 ; PixelSpacingBits = 40 
pixel_spacing_bits = -log2l( lPixelSpacing) = 40 so one can use float, but it can be slower then double 



Magnification = 8.589935e+09 ; radius = 1.164153e-10 ; PixelSpacing = 2.328306e-13 ; PixelSpacingBits = 41 
pixel_spacing_bits = -log2l( lPixelSpacing) = 41 so use double 
Magnification = 1.717987e+10 ; radius = 5.820766e-11 ; PixelSpacing = 1.164153e-13 ; PixelSpacingBits = 42 
pixel_spacing_bits = -log2l( lPixelSpacing) = 42 so use double 
Magnification = 3.435974e+10 ; radius = 2.910383e-11 ; PixelSpacing = 5.820766e-14 ; PixelSpacingBits = 43 
pixel_spacing_bits = -log2l( lPixelSpacing) = 43 so use double 
Magnification = 6.871948e+10 ; radius = 1.455192e-11 ; PixelSpacing = 2.910383e-14 ; PixelSpacingBits = 44 
pixel_spacing_bits = -log2l( lPixelSpacing) = 44 so use double 
Magnification = 1.374390e+11 ; radius = 7.275958e-12 ; PixelSpacing = 1.455192e-14 ; PixelSpacingBits = 45 
pixel_spacing_bits = -log2l( lPixelSpacing) = 45 so use double 
Magnification = 2.748779e+11 ; radius = 3.637979e-12 ; PixelSpacing = 7.275958e-15 ; PixelSpacingBits = 46 
pixel_spacing_bits = -log2l( lPixelSpacing) = 46 so use double 
Magnification = 5.497558e+11 ; radius = 1.818989e-12 ; PixelSpacing = 3.637979e-15 ; PixelSpacingBits = 47 
pixel_spacing_bits = -log2l( lPixelSpacing) = 47 so use double 
Magnification = 1.099512e+12 ; radius = 9.094947e-13 ; PixelSpacing = 1.818989e-15 ; PixelSpacingBits = 48 
pixel_spacing_bits = -log2l( lPixelSpacing) = 48 so use double 
Magnification = 2.199023e+12 ; radius = 4.547474e-13 ; PixelSpacing = 9.094947e-16 ; PixelSpacingBits = 49 
pixel_spacing_bits = -log2l( lPixelSpacing) = 49 so use double 
Magnification = 4.398047e+12 ; radius = 2.273737e-13 ; PixelSpacing = 4.547474e-16 ; PixelSpacingBits = 50 
pixel_spacing_bits = -log2l( lPixelSpacing) = 50 so use double 



Magnification = 8.796093e+12 ; radius = 1.136868e-13 ; PixelSpacing = 2.273737e-16 ; PixelSpacingBits = 51 
pixel_spacing_bits = -log2l( lPixelSpacing) = 51 so use long double 
Magnification = 1.759219e+13 ; radius = 5.684342e-14 ; PixelSpacing = 1.136868e-16 ; PixelSpacingBits = 52 
pixel_spacing_bits = -log2l( lPixelSpacing) = 52 so use long double 
Magnification = 3.518437e+13 ; radius = 2.842171e-14 ; PixelSpacing = 5.684342e-17 ; PixelSpacingBits = 53 
pixel_spacing_bits = -log2l( lPixelSpacing) = 53 so use long double 
Magnification = 7.036874e+13 ; radius = 1.421085e-14 ; PixelSpacing = 2.842171e-17 ; PixelSpacingBits = 54 
pixel_spacing_bits = -log2l( lPixelSpacing) = 54 so use long double 
Magnification = 1.407375e+14 ; radius = 7.105427e-15 ; PixelSpacing = 1.421085e-17 ; PixelSpacingBits = 55 
pixel_spacing_bits = -log2l( lPixelSpacing) = 55 so use long double 
Magnification = 2.814750e+14 ; radius = 3.552714e-15 ; PixelSpacing = 7.105427e-18 ; PixelSpacingBits = 56 
pixel_spacing_bits = -log2l( lPixelSpacing) = 56 so use long double 
Magnification = 5.629500e+14 ; radius = 1.776357e-15 ; PixelSpacing = 3.552714e-18 ; PixelSpacingBits = 57 
pixel_spacing_bits = -log2l( lPixelSpacing) = 57 so use long double 
Magnification = 1.125900e+15 ; radius = 8.881784e-16 ; PixelSpacing = 1.776357e-18 ; PixelSpacingBits = 58 
pixel_spacing_bits = -log2l( lPixelSpacing) = 58 so use long double 
Magnification = 2.251800e+15 ; radius = 4.440892e-16 ; PixelSpacing = 8.881784e-19 ; PixelSpacingBits = 59 
pixel_spacing_bits = -log2l( lPixelSpacing) = 59 so use long double 
Magnification = 4.503600e+15 ; radius = 2.220446e-16 ; PixelSpacing = 4.440892e-19 ; PixelSpacingBits = 60 
pixel_spacing_bits = -log2l( lPixelSpacing) = 60 so use long double 




Magnification = 9.007199e+15 ; radius = 1.110223e-16 ; PixelSpacing = 2.220446e-19 ; PixelSpacingBits = 61 
pixel_spacing_bits = -log2l( lPixelSpacing) = 61 so use arbitrary precision number (like MPFR) with 61 bits precision 
Magnification = 1.801440e+16 ; radius = 5.551115e-17 ; PixelSpacing = 1.110223e-19 ; PixelSpacingBits = 62 
pixel_spacing_bits = -log2l( lPixelSpacing) = 62 so use arbitrary precision number (like MPFR) with 62 bits precision 
Magnification = 3.602880e+16 ; radius = 2.775558e-17 ; PixelSpacing = 5.551115e-20 ; PixelSpacingBits = 63 
pixel_spacing_bits = -log2l( lPixelSpacing) = 63 so use arbitrary precision number (like MPFR) with 63 bits precision 
Magnification = 7.205759e+16 ; radius = 1.387779e-17 ; PixelSpacing = 2.775558e-20 ; PixelSpacingBits = 64 
pixel_spacing_bits = -log2l( lPixelSpacing) = 64 so use arbitrary precision number (like MPFR) with 64 bits precision 
Magnification = 1.441152e+17 ; radius = 6.938894e-18 ; PixelSpacing = 1.387779e-20 ; PixelSpacingBits = 65 
pixel_spacing_bits = -log2l( lPixelSpacing) = 65 so use arbitrary precision number (like MPFR) with 65 bits precision 
Magnification = 2.882304e+17 ; radius = 3.469447e-18 ; PixelSpacing = 6.938894e-21 ; PixelSpacingBits = 66 
pixel_spacing_bits = -log2l( lPixelSpacing) = 66 so use arbitrary precision number (like MPFR) with 66 bits precision 
Magnification = 5.764608e+17 ; radius = 1.734723e-18 ; PixelSpacing = 3.469447e-21 ; PixelSpacingBits = 67 
pixel_spacing_bits = -log2l( lPixelSpacing) = 67 so use arbitrary precision number (like MPFR) with 67 bits precision 
Magnification = 1.152922e+18 ; radius = 8.673617e-19 ; PixelSpacing = 1.734723e-21 ; PixelSpacingBits = 68 
pixel_spacing_bits = -log2l( lPixelSpacing) = 68 so use arbitrary precision number (like MPFR) with 68 bits precision 
Magnification = 2.305843e+18 ; radius = 4.336809e-19 ; PixelSpacing = 8.673617e-22 ; PixelSpacingBits = 69 
pixel_spacing_bits = -log2l( lPixelSpacing) = 69 so use arbitrary precision number (like MPFR) with 69 bits precision 
Magnification = 4.611686e+18 ; radius = 2.168404e-19 ; PixelSpacing = 4.336809e-22 ; PixelSpacingBits = 70 
pixel_spacing_bits = -log2l( lPixelSpacing) = 70 so use arbitrary precision number (like MPFR) with 70 bits precision 
Magnification = 9.223372e+18 ; radius = 1.084202e-19 ; PixelSpacing = 2.168404e-22 ; PixelSpacingBits = 71 
pixel_spacing_bits = -log2l( lPixelSpacing) = 71 so use arbitrary precision number (like MPFR) with 71 bits precision 
Magnification = 1.844674e+19 ; radius = 5.421011e-20 ; PixelSpacing = 1.084202e-22 ; PixelSpacingBits = 72 
pixel_spacing_bits = -log2l( lPixelSpacing) = 72 so use arbitrary precision number (like MPFR) with 72 bits precision 
Magnification = 3.689349e+19 ; radius = 2.710505e-20 ; PixelSpacing = 5.421011e-23 ; PixelSpacingBits = 73 
pixel_spacing_bits = -log2l( lPixelSpacing) = 73 so use arbitrary precision number (like MPFR) with 73 bits precision 
Magnification = 7.378698e+19 ; radius = 1.355253e-20 ; PixelSpacing = 2.710505e-23 ; PixelSpacingBits = 74 
pixel_spacing_bits = -log2l( lPixelSpacing) = 74 so use arbitrary precision number (like MPFR) with 74 bits precision 
Magnification = 1.475740e+20 ; radius = 6.776264e-21 ; PixelSpacing = 1.355253e-23 ; PixelSpacingBits = 75 
pixel_spacing_bits = -log2l( lPixelSpacing) = 75 so use arbitrary precision number (like MPFR) with 75 bits precision 
Magnification = 2.951479e+20 ; radius = 3.388132e-21 ; PixelSpacing = 6.776264e-24 ; PixelSpacingBits = 76 
pixel_spacing_bits = -log2l( lPixelSpacing) = 76 so use arbitrary precision number (like MPFR) with 76 bits precision 
Magnification = 5.902958e+20 ; radius = 1.694066e-21 ; PixelSpacing = 3.388132e-24 ; PixelSpacingBits = 77 
pixel_spacing_bits = -log2l( lPixelSpacing) = 77 so use arbitrary precision number (like MPFR) with 77 bits precision 
Magnification = 1.180592e+21 ; radius = 8.470329e-22 ; PixelSpacing = 1.694066e-24 ; PixelSpacingBits = 78 
pixel_spacing_bits = -log2l( lPixelSpacing) = 78 so use arbitrary precision number (like MPFR) with 78 bits precision 
Magnification = 2.361183e+21 ; radius = 4.235165e-22 ; PixelSpacing = 8.470329e-25 ; PixelSpacingBits = 79 
pixel_spacing_bits = -log2l( lPixelSpacing) = 79 so use arbitrary precision number (like MPFR) with 79 bits precision 
Magnification = 4.722366e+21 ; radius = 2.117582e-22 ; PixelSpacing = 4.235165e-25 ; PixelSpacingBits = 80 
pixel_spacing_bits = -log2l( lPixelSpacing) = 80 so use arbitrary precision number (like MPFR) with 80 bits precision 
Magnification = 9.444733e+21 ; radius = 1.058791e-22 ; PixelSpacing = 2.117582e-25 ; PixelSpacingBits = 81 
pixel_spacing_bits = -log2l( lPixelSpacing) = 81 so use arbitrary precision number (like MPFR) with 81 bits precision 
Magnification = 1.888947e+22 ; radius = 5.293956e-23 ; PixelSpacing = 1.058791e-25 ; PixelSpacingBits = 82 
pixel_spacing_bits = -log2l( lPixelSpacing) = 82 so use arbitrary precision number (like MPFR) with 82 bits precision 
Magnification = 3.777893e+22 ; radius = 2.646978e-23 ; PixelSpacing = 5.293956e-26 ; PixelSpacingBits = 83 
pixel_spacing_bits = -log2l( lPixelSpacing) = 83 so use arbitrary precision number (like MPFR) with 83 bits precision 
Magnification = 7.555786e+22 ; radius = 1.323489e-23 ; PixelSpacing = 2.646978e-26 ; PixelSpacingBits = 84 
pixel_spacing_bits = -log2l( lPixelSpacing) = 84 so use arbitrary precision number (like MPFR) with 84 bits precision 
Magnification = 1.511157e+23 ; radius = 6.617445e-24 ; PixelSpacing = 1.323489e-26 ; PixelSpacingBits = 85 
pixel_spacing_bits = -log2l( lPixelSpacing) = 85 so use arbitrary precision number (like MPFR) with 85 bits precision 
Magnification = 3.022315e+23 ; radius = 3.308722e-24 ; PixelSpacing = 6.617445e-27 ; PixelSpacingBits = 86 
pixel_spacing_bits = -log2l( lPixelSpacing) = 86 so use arbitrary precision number (like MPFR) with 86 bits precision 
Magnification = 6.044629e+23 ; radius = 1.654361e-24 ; PixelSpacing = 3.308722e-27 ; PixelSpacingBits = 87 
pixel_spacing_bits = -log2l( lPixelSpacing) = 87 so use arbitrary precision number (like MPFR) with 87 bits precision 
Magnification = 1.208926e+24 ; radius = 8.271806e-25 ; PixelSpacing = 1.654361e-27 ; PixelSpacingBits = 88 
pixel_spacing_bits = -log2l( lPixelSpacing) = 88 so use arbitrary precision number (like MPFR) with 88 bits precision 
Magnification = 2.417852e+24 ; radius = 4.135903e-25 ; PixelSpacing = 8.271806e-28 ; PixelSpacingBits = 89 
pixel_spacing_bits = -log2l( lPixelSpacing) = 89 so use arbitrary precision number (like MPFR) with 89 bits precision 
Magnification = 4.835703e+24 ; radius = 2.067952e-25 ; PixelSpacing = 4.135903e-28 ; PixelSpacingBits = 90 
pixel_spacing_bits = -log2l( lPixelSpacing) = 90 so use arbitrary precision number (like MPFR) with 90 bits precision 
Magnification = 9.671407e+24 ; radius = 1.033976e-25 ; PixelSpacing = 2.067952e-28 ; PixelSpacingBits = 91 
pixel_spacing_bits = -log2l( lPixelSpacing) = 91 so use arbitrary precision number (like MPFR) with 91 bits precision 
Magnification = 1.934281e+25 ; radius = 5.169879e-26 ; PixelSpacing = 1.033976e-28 ; PixelSpacingBits = 92 
pixel_spacing_bits = -log2l( lPixelSpacing) = 92 so use arbitrary precision number (like MPFR) with 92 bits precision 
Magnification = 3.868563e+25 ; radius = 2.584939e-26 ; PixelSpacing = 5.169879e-29 ; PixelSpacingBits = 93 
pixel_spacing_bits = -log2l( lPixelSpacing) = 93 so use arbitrary precision number (like MPFR) with 93 bits precision 
Magnification = 7.737125e+25 ; radius = 1.292470e-26 ; PixelSpacing = 2.584939e-29 ; PixelSpacingBits = 94 
pixel_spacing_bits = -log2l( lPixelSpacing) = 94 so use arbitrary precision number (like MPFR) with 94 bits precision 
Magnification = 1.547425e+26 ; radius = 6.462349e-27 ; PixelSpacing = 1.292470e-29 ; PixelSpacingBits = 95 
pixel_spacing_bits = -log2l( lPixelSpacing) = 95 so use arbitrary precision number (like MPFR) with 95 bits precision 
Magnification = 3.094850e+26 ; radius = 3.231174e-27 ; PixelSpacing = 6.462349e-30 ; PixelSpacingBits = 96 
pixel_spacing_bits = -log2l( lPixelSpacing) = 96 so use arbitrary precision number (like MPFR) with 96 bits precision 
Magnification = 6.189700e+26 ; radius = 1.615587e-27 ; PixelSpacing = 3.231174e-30 ; PixelSpacingBits = 97 
pixel_spacing_bits = -log2l( lPixelSpacing) = 97 so use arbitrary precision number (like MPFR) with 97 bits precision 
Magnification = 1.237940e+27 ; radius = 8.077936e-28 ; PixelSpacing = 1.615587e-30 ; PixelSpacingBits = 98 
pixel_spacing_bits = -log2l( lPixelSpacing) = 98 so use arbitrary precision number (like MPFR) with 98 bits precision 
Magnification = 2.475880e+27 ; radius = 4.038968e-28 ; PixelSpacing = 8.077936e-31 ; PixelSpacingBits = 99 
pixel_spacing_bits = -log2l( lPixelSpacing) = 99 so use arbitrary precision number (like MPFR) with 99 bits precision 
Magnification = 4.951760e+27 ; radius = 2.019484e-28 ; PixelSpacing = 4.038968e-31 ; PixelSpacingBits = 100 
pixel_spacing_bits = -log2l( lPixelSpacing) = 100 so use arbitrary precision number (like MPFR) with 100 bits precision 
Magnification = 9.903520e+27 ; radius = 1.009742e-28 ; PixelSpacing = 2.019484e-31 ; PixelSpacingBits = 101 
pixel_spacing_bits = -log2l( lPixelSpacing) = 101 so use arbitrary precision number (like MPFR) with 101 bits precision 
Magnification = 1.980704e+28 ; radius = 5.048710e-29 ; PixelSpacing = 1.009742e-31 ; PixelSpacingBits = 102 
pixel_spacing_bits = -log2l( lPixelSpacing) = 102 so use arbitrary precision number (like MPFR) with 102 bits precision 
Magnification = 3.961408e+28 ; radius = 2.524355e-29 ; PixelSpacing = 5.048710e-32 ; PixelSpacingBits = 103 
pixel_spacing_bits = -log2l( lPixelSpacing) = 103 so use arbitrary precision number (like MPFR) with 103 bits precision 
Magnification = 7.922816e+28 ; radius = 1.262177e-29 ; PixelSpacing = 2.524355e-32 ; PixelSpacingBits = 104 
pixel_spacing_bits = -log2l( lPixelSpacing) = 104 so use arbitrary precision number (like MPFR) with 104 bits precision 
Magnification = 1.584563e+29 ; radius = 6.310887e-30 ; PixelSpacing = 1.262177e-32 ; PixelSpacingBits = 105 
pixel_spacing_bits = -log2l( lPixelSpacing) = 105 so use arbitrary precision number (like MPFR) with 105 bits precision 

```
